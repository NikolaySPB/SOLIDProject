﻿using System;

namespace SOLIDProject.Converter
{
    /// <summary>
    /// Класс выполняет конвертацию долларов в рубли
    /// </summary>
    class ConvectorUsdRub : IConvector
    {
        public double Calculate(double money, double usd)
            => Math.Round(usd * money, 2);

        public string GetOfCurrency()
            => "USD";

        public string GetInCurrency()
            => "RUB";
    }
}
