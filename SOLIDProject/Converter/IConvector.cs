﻿namespace SOLIDProject.Converter
{
    /// <summary>
    /// Интерфейс для конвертеров
    /// </summary>
    interface IConvector
    {
        double Calculate(double money, double usd);

        string GetOfCurrency();

        string GetInCurrency();
    }
}
