﻿namespace SOLIDProject.Converter
{
    class Convector
    {
        readonly IConvector convector;
        public string InCurrency { get => GetInCurrency(); }
        public string OfCurrency { get => GetOfCurrency(); }

        public Convector(IConvector convector) 
            => this.convector = convector;

        public double Convert(double money, double usd)
            => convector.Calculate(money, usd);        

        private string GetInCurrency() 
            => convector.GetInCurrency();        

        private string GetOfCurrency() 
            => convector.GetOfCurrency();        
    }
}
