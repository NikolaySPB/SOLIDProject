﻿using System;

namespace SOLIDProject.Converter
{
    /// <summary>
    /// Класс выполняет конвертацию рублей в доллары
    /// </summary>
    class ConvectorRubUsd : IConvector
    {       
        public double Calculate(double money, double usd)
            => Math.Round(money / usd, 2);

        public string GetOfCurrency()
            => "RUB";

        public string GetInCurrency()
            => "USD";
    }
}
