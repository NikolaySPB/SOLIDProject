﻿using System.Net;

namespace SOLIDProject.Parsing
{
    /// <summary>
    /// Класс выполняет парсинг url адреса, и сохраняет страницу в свойство HTML, в строковом значении.
    /// </summary>
    class YandexUrlParsing : IUrlParsing
    {
        private const string url = @"https://yandex.ru/";

        WebClient webClient;
             
        public string GetHtml()
        {
            webClient = new WebClient();
            string html = webClient.DownloadString(url);
            return html;
        }
    }

}
