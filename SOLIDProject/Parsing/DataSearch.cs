﻿using System;
using System.Text.RegularExpressions;

namespace SOLIDProject.Parsing
{
    /// <summary>
    /// Класс выполняет поиск текущего курса доллара с помощью регулярного выражения и сохраняет значение найденного результата в свойство USD.
    /// </summary>
    class DataSearch
    {
        private const string regularExpression = @"USD.*?value_inner"">(.+?)<\/span>";
        readonly IUrlParsing url;

        public DataSearch(IUrlParsing url) => this.url = url; 

        public double Search()
        {
            string html = url.GetHtml();
            Match match = Regex.Match(html, regularExpression);
            string value = match.Groups[1].Value;
            double usd = Convert.ToDouble(value);
            return usd;
        }
    }
}
