﻿using System.IO;

namespace SOLIDProject.InputOutput
{
    /// <summary>
    /// Класс выполняет запись в файл
    /// </summary>
    class DataWriteFile : DataWrite
    {
        readonly string fileName;

        public DataWriteFile(IDataWrite write, string fileName) 
            : base (write) => this.fileName = fileName;        

        public override void Write(string text)
            => File.WriteAllText(fileName, text);      
    }
}
