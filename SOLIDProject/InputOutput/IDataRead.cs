﻿namespace SOLIDProject.InputOutput
{
    /// <summary>
    /// Интерфейс для чтения с консоли
    /// </summary>
    interface IDataRead
    {
        double Read();
    }
}
