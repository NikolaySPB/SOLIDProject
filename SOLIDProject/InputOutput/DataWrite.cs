﻿using System;

namespace SOLIDProject.InputOutput
{
    class DataWrite : IDataWrite
    {
        readonly IDataWrite write;
        public DataWrite(IDataWrite write)
        {
            this.write = write;
        }

        public virtual void Write(string text)
        {
            write.Write(text);
        }
    }
}
