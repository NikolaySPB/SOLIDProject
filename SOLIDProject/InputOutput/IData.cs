﻿namespace SOLIDProject.InputOutput
{
    /// <summary>
    /// Интерфейс для класса работающего с консолью
    /// </summary>
    interface IData : IDataWrite, IDataRead
    {
        double Input(string text);

        void End();
    }
}
