﻿namespace SOLIDProject.InputOutput
{
    /// <summary>
    /// Интерфейс для классов записи (в консоль или в файл)
    /// </summary>
    interface IDataWrite
    {               
        void Write(string text);
    }
}
