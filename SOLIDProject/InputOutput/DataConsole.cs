﻿using System;

namespace SOLIDProject.InputOutput
{
    /// <summary>
    /// Класс выполняет работу с консолью: запись, чтение.
    /// </summary>
    class DataConsole : IData
    {
        public double Input(string text)
        {
            Write(text);
            return Read();
        }

        public double Read()
        {
            string val = Console.ReadLine();
            bool isNum = double.TryParse(val, out _);
            if (isNum)
            {
                if (val.IndexOf('.') != -1)
                    val = val.Replace(".", ",");
                return double.Parse(val);
            }
            else
            {
                Write("Введено не числовое значение!\nПопробуйте снова.");
                return Read();
            }            
        }

        public void Write(string text)
            => Console.WriteLine(text);

        public void End()
            => Console.ReadLine();
    }
}
