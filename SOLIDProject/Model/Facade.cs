﻿using SOLIDProject.Converter;
using SOLIDProject.InputOutput;
using SOLIDProject.Parsing;

namespace SOLIDProject.Model
{
    /// <summary>
    /// Класс выполняющий реализацию приложения
    /// </summary>
    class Facade
    {
        readonly DataSearch dataSearch;
        readonly Convector convector;
        readonly IData data;
        readonly IDataWrite dataWrite;

        public Facade(DataSearch dataSearch, Convector convector, IData data, IDataWrite dataWrite)
        {
            this.data = data;
            this.dataWrite = dataWrite;
            this.convector = convector;
            this.dataSearch = dataSearch;
        }

        public Facade(DataSearch dataSearch, Convector convector, IData data)
            : this(dataSearch, convector, data, data) { }

        public void Run()
        {
            data.Write("КОНВЕКТОР ВОЛЮТ USD/RUB");
            double usd = dataSearch.Search();
            data.Write($"Текущий курс: 1 USD = {usd} RUB");
            double money = data.Input($"Введите сумму {convector.OfCurrency}, которую нужно конвертировать в {convector.InCurrency}:");
            double resuil = convector.Convert(money, usd);
            dataWrite.Write($"Введенная сумма {money} {convector.OfCurrency} будет ровна: {resuil} {convector.InCurrency}");
            data.End();
        }
    }
}
