﻿using SOLIDProject.Converter;
using SOLIDProject.InputOutput;
using SOLIDProject.Model;
using SOLIDProject.Parsing;

namespace SOLIDProject
{
    class Program
    {
        static void Main(string[] args)
        {
            IUrlParsing yandexUrl = new YandexUrlParsing();
            DataSearch dataSearch = new DataSearch(yandexUrl);
            IData dataConsole = new DataConsole();
            IConvector rub = new ConvectorRubUsd();
            Convector convector = new Convector(rub);


            #region Первый вариант работы класса Facade

            Facade facade = new Facade(dataSearch, convector, dataConsole);
            facade.Run();

            #endregion


            #region Этот вариант будет использовать другой конвертер

            IConvector usd = new ConvectorUsdRub();
            Convector convector2 = new Convector(usd);
            Facade facade1 = new Facade(dataSearch, convector2, dataConsole);
            facade1.Run();

            #endregion


            #region Данный вариант с подстановкой Барбары Лисков запишет конечный результат в файл  

            DataWrite dataWriteFile = new DataWriteFile(dataConsole, "result.txt");
            Facade facade2 = new Facade(dataSearch, convector, dataConsole, dataWriteFile);
            facade2.Run();

            #endregion
        }
    }
}
